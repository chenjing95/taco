#include <iostream>
#include "taco.h"

using namespace taco;

int main(int argc, char* argv[]) {
  // Create formats
  Format csr({Dense,Sparse});
  Format ddm({Dense,Dense});
  Format ssm({Sparse,Sparse});
  Format csf({Sparse,Sparse,Sparse});
  Format  sv({Sparse});

  // Create tensors
  Tensor<double> A({4,3},   ssm);
  Tensor<double> B({4,3},   ssm);
  Tensor<double> C({4,2},   ddm);
  Tensor<double> D({2,3},   ddm);

  // Insert data into B and c
  B.insert({0,0}, 1.0);
  B.insert({1,1}, 1.0);
  B.insert({3,0}, 1.0);
  B.insert({3,2}, 1.0);

  C.insert({0,0}, 1.0);
  C.insert({0,1}, 2.0);
  C.insert({1,0}, 3.0);
  C.insert({1,1}, 4.0);
  C.insert({2,0}, 5.0);
  C.insert({2,1}, 6.0);
  C.insert({3,0}, 7.0);
  C.insert({3,1}, 8.0);

  D.insert({0,0}, 1.0);
  D.insert({0,1}, 2.0);
  D.insert({0,2}, 3.0);
  D.insert({1,0}, 4.0);
  D.insert({1,1}, 5.0);
  D.insert({1,2}, 6.0);

  // Pack data as described by the formats
  B.pack();
  C.pack();
  D.pack();

  // Form a tensor-vector multiplication expression
  IndexVar i, j, k;
  A(i,j) = C(i,k) * D(k,j) * B(i,j);

  // Compile the expression
  A.compile();

  // Assemble A's indices and numerically compute the result
  A.assemble();
  A.compute();

  std::cout << A << std::endl;
}
