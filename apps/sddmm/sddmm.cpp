// On Linux and MacOS, you can compile and run this program like so:
// //   g++ -std=c++11 -O3 -DNDEBUG -DTACO -I ../../include -L../../build/lib -ltaco sddmm.cpp -o sddmm
// //   LD_LIBRARY_PATH=../../build/lib ./sddmm
//
#include <random>
#include <time.h>
#include <sys/time.h>

#include "taco.h"

using namespace taco;

double gettime()
{
    struct timeval t;
    gettimeofday(&t,NULL);
    return t.tv_sec+t.tv_usec*1e-6;
    return 1;
}

int main(int argc, char* argv[]) 
{
  for(int cj = 10; cj<=100; cj+=10)
  {
  std::default_random_engine gen(0);
  std::uniform_real_distribution<double> unif(0.0, 1.0);

  Format dcsr({Sparse,Sparse});
  Format   rm({Dense,Dense});
  Format   cm({Dense,Dense}, {1,0});

  Tensor<double> B = read("/home/chenjing/dataset/delicious/bookmark_tags.mtx", dcsr);

  Tensor<double> C({B.getDimension(0), cj}, rm);
  for (int i = 0; i < C.getDimension(0); ++i) {
    for (int j = 0; j < C.getDimension(1); ++j) {
      C.insert({i,j}, unif(gen));
    }
  }
  C.pack();

  Tensor<double> D({cj, B.getDimension(1)}, cm);
  for (int i = 0; i < D.getDimension(0); ++i) {
    for (int j = 0; j < D.getDimension(1); ++j) {
      D.insert({i,j}, unif(gen));
    }
  }
  D.pack();

  Tensor<double> A(B.getDimensions(), dcsr);

  	IndexVar i, j, k;
  	A(i,j) = B(i,j) * C(i,k) * D(k,j);

  	A.compile();
  	A.assemble();
  	double t1=gettime();
  	A.compute();
  	double t2=gettime();
	double T = t2 - t1;
  	printf("when k = %d, SDDMM Time:%lf s.\n", cj, T);
  }
  //write("A.mtx", A);
}
